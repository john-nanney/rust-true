
use clap::Parser;

#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
struct Args {

    #[arg(short, long, default_value = "")]
    message: String,

    #[arg(short, long, default_value_t = 0)]
    code: u8,

    #[arg(short, long)]
    help: bool,
}

fn main() {

    let args = Args::parse();

    if args.help {
        std::process::exit(254);
    }

    println!("{}", args.message);

    std::process::exit(args.code.into());
}
