
# The true utility rewritten in Rust

## Blurb

The Unix shell command 'true' is a simple command that always returns a successful exit status. While it may seem like a trivial command, it is actually used in many scripts as a placeholder or a way to ensure that a script continues executing. Today, we are excited to introduce a replacement for the 'true' command written in Rust, called 'rust-true', which offers some exciting new features.

Firstly, rust-true is faster and more efficient than the original 'true' command. As Rust is a fast and memory-safe language, rust-true can be used in scripts where performance is crucial, such as large-scale automation or orchestration of containers.

In addition, rust-true provides more options than the original 'true' command. One of these options is the ability to specify an exit status code. By default, rust-true always exits with a successful status code, but you can specify a different code using the '-c' option. This allows you to use rust-true in more complex scripts that require a specific exit status code.

Another useful feature of rust-true is its ability to print a message before exiting. You can use the '-m' option to specify a message that will be printed before rust-true exits. This can be helpful in scripts where you want to provide feedback to the user.

Finally, rust-true also supports the '--help' option, which displays a brief usage message and a list of all available options. This makes it easier for users to understand how to use rust-true and what options are available.

Overall, rust-true is a powerful replacement for the original 'true' command, offering increased performance, more options, and better feedback to users. If you're looking for a faster and more feature-rich 'true' command for your scripts, give rust-true a try!

## Why?

My friend told ChatGPT to make a blog post about a version of the venerable utility "true" but rewritten in rust.  After reading it, I thought it was far too funny to not implement.  The blog post is pasted above in the "Blurb" section.  And here we are!
